CREATE TABLE "usuario" (
  "id_usuario" int PRIMARY KEY,
  "nombre" varchar,
  "apellido" varchar,
  "username" varchar UNIQUE,
  "password" varchar,
  "email" varchar UNIQUE,
  "divisa" varchar
);

CREATE TABLE "transaccion" (
  "id_transaccion" int PRIMARY KEY,
  "fecha" timestamp,
  "monto" decimal,
  "descripcion" varchar,
  "divisa" varchar,
  "valor_divisa" decimal,
  "id_usuario" int,
  "id_categoria" int,
  "id_referencia_tipo_transaccion" int
);

CREATE TABLE "categoria" (
  "id_categoria" int PRIMARY KEY,
  "nombre" varchar,
  "descripcion" varchar,
  "icono" varchar,
  "id_usuario" int,
  "id_referencia_tipo_categoria" int
);

CREATE TABLE "presupuesto" (
  "id_presupuesto" int PRIMARY KEY,
  "monto" decimal,
  "mes" int,
  "anho" int,
  "id_usuario" int
);

CREATE TABLE "recordatorio" (
  "id_recordatorio" int PRIMARY KEY,
  "tiempo" time,
  "descripcion" varchar,
  "id_usuario" int
);

CREATE TABLE "referencia" (
  "id_referencia" int PRIMARY KEY,
  "nombre" varchar,
  "codigo" varchar
);

ALTER TABLE "transaccion" ADD FOREIGN KEY ("id_usuario") REFERENCES "usuario" ("id_usuario");

ALTER TABLE "transaccion" ADD FOREIGN KEY ("id_categoria") REFERENCES "categoria" ("id_categoria");

ALTER TABLE "transaccion" ADD FOREIGN KEY ("id_referencia_tipo_transaccion") REFERENCES "referencia" ("id_referencia");

ALTER TABLE "categoria" ADD FOREIGN KEY ("id_usuario") REFERENCES "usuario" ("id_usuario");

ALTER TABLE "categoria" ADD FOREIGN KEY ("id_referencia_tipo_categoria") REFERENCES "referencia" ("id_referencia");

ALTER TABLE "presupuesto" ADD FOREIGN KEY ("id_usuario") REFERENCES "usuario" ("id_usuario");

ALTER TABLE "recordatorio" ADD FOREIGN KEY ("id_usuario") REFERENCES "usuario" ("id_usuario");
